import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.Scanner;

import controller.MenuController;
import controller.WelcomeController;

public class FileSystemManager {
    /**
     * Provide user with the menu:
     *      - list all files
     *      - list all files by extension
     *          - provide possible extensions
     * Requirements:
     *      - menu should persist
     *      - there should be an option to exit  
     */

    public static void main(String[] args) throws InvalidPathException, SecurityException, IOException, IllegalArgumentException {

        final WelcomeController welcomeController = new WelcomeController();
        final MenuController menuController = new MenuController();

        Scanner console = new Scanner(System.in);
        welcomeController.greeting();
        welcomeController.getUserName(console);

        menuController.displayMainMenu();
        menuController.selectMenuItem(console);
    }
}