package exceptions.userExceptions;

public class InvalidUserSelectionException extends Exception{
    public InvalidUserSelectionException() {
        super(
            "Invalid selection." + "\n" +
            "Please, try again.");
    }
}
