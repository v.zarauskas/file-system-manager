package exceptions.userExceptions;

public class NameExistsException extends Exception{
    public NameExistsException() {
        super(
            "The input name already exists." + "\n" +
            "Please, try again.");
    }
}
