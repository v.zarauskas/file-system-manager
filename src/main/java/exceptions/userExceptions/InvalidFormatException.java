package exceptions.userExceptions;

public class InvalidFormatException extends Exception{
    public InvalidFormatException() {
        super(
            "The input name consists of non-alphabetic characters." + "\n" +
            "Please, try again and use regular alphabet characters.");
    }
}
