package exceptions.userExceptions;

public class EmptyNameException extends Exception{
    public EmptyNameException() {
        super(
            "The input name is empty." + "\n" +
            "Please, try again.");
    }
}
