package exceptions.userExceptions;

public class NameTooLongException extends Exception{
    public NameTooLongException() {
        super(
            "The input name has too many characters." + "\n" +
            "Please, try again and limit your name to 20 characters.");
    }
}
