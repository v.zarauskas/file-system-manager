package model;

import java.nio.file.Path;

public class FileModel {
    private Path path;
    private String name;
    private String extension;
    
    public FileModel(Path path) throws IllegalArgumentException {
        this.path = path;
        this.name = path.getName(path.getNameCount()-1).toString();
        this.extension = getNameExtension();
    }

    public Path getPath() {
        return this.path;
    }

    public String getName() {
        return this.name;
    }

    public String getExtension() {
        return this.extension;
    }
    
    private String getNameExtension() {
        return this.name.substring(this.name.lastIndexOf("."));
    }
}
