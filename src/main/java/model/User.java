package model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class User {
    private static final Map<Long, String> sessionUsers = new HashMap<>();
    private static AtomicLong idCounter = new AtomicLong(); // for generating unique ID per JVM run

    private long id;
    private String name;

    public User(String name) {
        this.id = createID();
        this.name = name;
        sessionUsers.put(id, name);
    }
    
    private static synchronized long createID() {
        return idCounter.getAndIncrement();
    
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public static Map<Long, String> getUsers() {
        return sessionUsers;
    }
}
