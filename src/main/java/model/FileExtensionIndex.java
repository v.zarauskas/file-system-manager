package model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileExtensionIndex {
    private Map<String, List<File>> index;

    public FileExtensionIndex() {
        this.index = new HashMap<String, List<File>>();
    }

    public Map<String, List<File>> getExtensionIndex() {
        return index;
    }

    public void addExtension(FileModel inputFile) {
        String extension = inputFile.getExtension();
        try {
            File file = new File(inputFile.getPath().toString());
            boolean containsExtension = index.containsKey(extension);
            if(containsExtension) {
                this.index.get(extension).add(file);
            } else {
                List<File> fileNames = new ArrayList<>();
                fileNames.add(file);
                this.index.put(extension, fileNames);
            }
        } catch (NullPointerException e) {
            //TODO: handle exception
        }
    }
    
    // Used for testing
    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        index.forEach((k, v) -> {
            build
            .append(k + ": ")
            .append(listOfFilesToString(v))
            .append("\n");
        });
        return build.toString();
    }

    public String listOfFilesToString(List<File> listOfFiles) {
        if (listOfFiles.isEmpty()) {
            return "There are no files of this type";
        }

        // For the following, we could utilize iterator for performance if LinkedList were passed in.
        // However, by construction it is ArrayList, so the code is a little bit simpler
        StringBuilder build = new StringBuilder("[" + listOfFiles.get(0).getName());
        for (int i = 1; i < listOfFiles.size(); i++) {
            build.append(", ").append(listOfFiles.get(i).getName());
        }
        build.append("]");
        return build.toString();
    }
}
