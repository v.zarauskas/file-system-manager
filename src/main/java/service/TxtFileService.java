package service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

public class TxtFileService {
    /**
     * Requirements:
     *     <<SEARCH IGNORES CASE>>
     *     3.1. public String getTxtFileName() - the design of the application allows to achieve this using java libriaries
     *     3.2. public long getFileSize(String fileName) - the design of the application allows to achieve this using java libriaries
     *     3.3. public int calculateNrOfLines(String fileName)
     *     3.4. public boolean containsWord(String word)
     *     3.5. public int countOccurenceOfAWord(String word)
     */

    private File file;

    public TxtFileService(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public int calculateNrOfLines() {
        int nrOfLines = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while (bufferedReader.readLine() != null) {
                nrOfLines++;
            }
            bufferedReader.close();
        } catch (IOException e) {
            //TODO: handle exception
        }
        return nrOfLines;
    }

    // Add functionality fo suggest words of the same root if the word itself in
    // it's form was not found
    public boolean containsWord(String word) {
        try (FileReader reader = new FileReader(file)) {
            StreamTokenizer tokenizer = new StreamTokenizer(reader);
            while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
                if (tokenizer.ttype == StreamTokenizer.TT_WORD && tokenizer.sval.equalsIgnoreCase(word)) {
                    return true;
                }
            }
        } catch (IOException e) {
            //TODO: handle exception
        }
        return false;
    }

    public int countOccurenceOfAWord(String word) {
        int wordCount = 0;
        try (FileReader reader = new FileReader(file)) {
            StreamTokenizer tokenizer = new StreamTokenizer(reader);
            while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
                if (tokenizer.ttype == StreamTokenizer.TT_WORD && tokenizer.sval.equalsIgnoreCase(word)) {
                    wordCount++;
                }
            }
        } catch (IOException e) {
            //TODO: handle exception
        }
        return wordCount;       
    }
}
