package service;

import model.FileModel;
import model.FileExtensionIndex;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.stream.Stream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.InvalidPathException;
import java.lang.SecurityException;
import java.io.File;
import java.io.IOException;

public class FileService {
/**
 * Requirements:
 *      <<SEARCH IGNORES CASE>>
 *      1. public List<String> listAllFileNames
 *      2. public List<String> listAllFileNamseByExtension(String extension)
 *      3. txtFileService
 */
  
    private FileExtensionIndex index;
    private List<FileModel> files;

    public FileService() {
        this.index = new FileExtensionIndex();
        this.files = getFiles();
    }
    
    public List<String> listAllFileNames() {
        List<String> fileNames = new ArrayList<>();
        try {
            files.forEach(file -> {
                fileNames.add(file.getName()); // Could it be written using "::"?
            });    
        } catch (IllegalArgumentException e) {
            //TODO: handle exception
        }
        return fileNames;
    }

    public String listAllFileNamesByExtension(String extension) {
        return listOfFilesNamesToString(listAllFilesByExtension(extension));
    }

    public List<File> listAllFilesByExtension(String extension) {
        return index.getExtensionIndex().get(extension);
    }

    public Set<String> listAllFileExtensions() {
        return index.getExtensionIndex().keySet();
    }

    public List<FileModel> getFiles() {
        List<FileModel> files = new ArrayList<>();
        try {
            Path locationOfFiles = Paths.get(System.getProperty("user.dir") + "/src/main/resources/files");
            try (Stream<Path> paths = Files.walk(locationOfFiles)) {
                paths
                    .filter(Files::isRegularFile)
                    .forEach(filePath -> {
                        FileModel newFile = new FileModel(filePath);
                        files.add(newFile);
                        this.index.addExtension(newFile);
                    });
            } catch (IOException e) {
                //TODO: handle exception
            } catch (IllegalArgumentException e) {
                //TODO: handle exception
            }
        } catch (InvalidPathException e) {
            //TODO: handle exception
        } catch (IllegalArgumentException e) {
            //TODO: handle exception
        } catch (NullPointerException e) {
            //TODO: handle exception
        } catch (SecurityException e) {
            //TODO: handle exception
        }
        return files;
    }

    public String listOfExtensionsToString(Set<String> extensions) {
        StringBuilder build = new StringBuilder();
        for (String extension : extensions) {
            build.append(extension).append("\n");
        }
        return build.toString();
    }

    public String listOfFilesNamesToString(List<File> files) {
        StringBuilder build = new StringBuilder();
        for (File file : files) {
            build.append(file.getName()).append("\n");
        }
        return build.toString();
    }
    
    public FileExtensionIndex getIndex() {
        return index;
    }
}

