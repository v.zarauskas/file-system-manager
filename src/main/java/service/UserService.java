package service;

import java.util.Set;
import java.util.List;
import java.io.File;

import exceptions.userExceptions.*;
import model.User;

public class UserService {
    public boolean validateUserName(String name)
        throws EmptyNameException, NameTooLongException, InvalidFormatException, NameExistsException {
        boolean isNameEmpty = name.length() == 0;
        boolean isNameTooLong = name.length() > 20;
        boolean userNameExists = User.getUsers().values().contains(name);
        if (isNameEmpty) {
            throw new EmptyNameException();
        } else if (isNameTooLong) {
            throw new NameTooLongException();
        } else if (!isAlphabetic(name)) {
            throw new InvalidFormatException();
        } else if (userNameExists) {
            throw new NameExistsException();
        }
        return true;
    }

    public boolean validateUserWordInput(String word) throws InvalidFormatException {
        if (!isAlphabetic(word)) {
            throw new InvalidFormatException();
        }
        return true;
    }

    public boolean validateUserMenuItemSelection(String selectedMenuItem, String menuOptions)
        throws InvalidUserSelectionException {
        boolean isValidSelection = selectedMenuItem.length() == 1 && menuOptions.contains(selectedMenuItem);
        if (!isValidSelection) {
            throw new InvalidUserSelectionException(); 
        }
        return true;
    }

    public boolean validateUserExtensionSelection(String selectedExtension, Set<String> availableExtensions)
        throws InvalidUserSelectionException {
        boolean isValidSelection = availableExtensions.contains(selectedExtension);
        if (!isValidSelection) {
            throw new InvalidUserSelectionException(); 
        }
        return true;
    }

    public boolean validateUserFileNameSelection(String selectedFileName, List<File> availableFiles)
        throws InvalidUserSelectionException {
        boolean isValidSelection = containsFile(selectedFileName, availableFiles);
        if (!isValidSelection) {
            throw new InvalidUserSelectionException(); 
        }
        return true;
    }

    private boolean isAlphabetic(String input) {
        char[] chars = input.toCharArray();
        for (char c : chars) {
            if (!Character.isAlphabetic((int) c)) {
                return false;
            }
        }
        return true;
    }

    private boolean containsFile(String selectedFileName, List<File> availableFiles) {
        for (int i = 0; i < availableFiles.size(); i++) {
            File currFile = availableFiles.get(i);
            if (currFile.getName().equals(selectedFileName)) {
                return true;
            }
        }
        return false;
    }
}
