package controller;

import java.util.NoSuchElementException;
import java.util.Scanner;

import exceptions.userExceptions.*;
import model.User;
import service.UserService;

public class WelcomeController {
    private UserService userService = new UserService();

    public void greeting() {
        System.out.println(
            "---------------------------------------------------------------------------------------" + "\n" +
            "|                          Welcome to File System Manager                             |" + "\n" +
            "---------------------------------------------------------------------------------------" + "\n" +
            "\n" +
            "This appplication allows you to manage and manipulate your files." + "\n"
        );
    }

    public void getUserName(Scanner console) {
        String name = null;
        boolean isValidNameEntered = false;
        while (!isValidNameEntered) {
            try {                
                System.out.println(
                    "Enter your name in the form [Name] or, to exit the program, enter [q] or [Q]:");
                name = console.nextLine();
                System.out.println();

                if (name.equalsIgnoreCase("q")) {
                    System.exit(0);
                } else if (userService.validateUserName(name)) {
                    new User(name);
                    System.out.println("Hello, " + name + "!");
                }

                isValidNameEntered = true;

            } catch (EmptyNameException e) {
                System.err.println(e.getMessage());
            } catch (NameTooLongException e) {
                System.err.println(e.getMessage());
            } catch (InvalidFormatException e) {
                System.err.println(e.getMessage());
            } catch (NameExistsException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
    }
}
