package controller;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

import exceptions.userExceptions.InvalidUserSelectionException;
import service.FileService;
import service.UserService;

public class MenuController {

    WelcomeController welcomeController = new WelcomeController();
    UserService userService = new UserService();
    FileService fileService = new FileService();

    public void displayMainMenu() {
        System.out.println(
            "You have the following options:" + "\n" +
            "   [q] or [Q] - exit the program" + "\n" +
            "   [s] - switch user" + "\n" +
            "   [f] - list all files" + "\n" +
            "   [x] - list all files by extension" + "\n" +
            "   [t] - explore .txt files"
        );
    }

    public void selectMenuItem(Scanner console)  {
        System.out.println(
            "Select an option:"
        );

        String selectedMenuItem = null;
        boolean isValidMenuItemSelected = false;
        while (!isValidMenuItemSelected) {
            try {
                String menuOptions = "sfxt";
                selectedMenuItem = console.nextLine();
                System.out.println();

                if (selectedMenuItem.equalsIgnoreCase("q")) {
                    System.exit(0);
                } else if (userService.validateUserMenuItemSelection(selectedMenuItem, menuOptions)) {
                    switch (selectedMenuItem) {
                        case "s":
                            switchUser(console);
                            break;
                        case "f":
                            printAllFiles(console);
                            break;
                        case "x":
                            listAllFilesByExtension(console);
                            break;
                        case "t":
                            exploreTxtFiles(console);
                            break;
                    }
                }
                
                isValidMenuItemSelected = true;
            } catch (InvalidUserSelectionException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
    }

    private void switchUser(Scanner console) {
        System.out.println("\nThe previous session is over.");
        welcomeController.getUserName(console);
        displayMenuAndRepromt(console);
    }

    private void printAllFiles(Scanner console) {
        System.out.println("\nThe following are all the files:");
        List<String> allFiles = fileService.listAllFileNames();
        for (String file : allFiles) {
            System.out.println(file);
        }
        System.out.println();
        displayMenuAndRepromt(console);
    }

    private void listAllFilesByExtension(Scanner console) {
        System.out.println("\nThe following are all present file types:");
        Set<String> extensions = fileService.listAllFileExtensions();
        System.out.println(fileService.listOfExtensionsToString(extensions));
        selectExtension(console, extensions);
        displayMenuAndRepromt(console);
    }

    private void selectExtension(Scanner console, Set<String> availableExtensions) {
        System.out.println(
            "Select one of the above:"
        );

        String selectedExtension = null;
        boolean isValidSelection = false;
        while (!isValidSelection) {
            try {
                selectedExtension = console.nextLine();
                System.out.println();

                if (userService.validateUserExtensionSelection(selectedExtension, availableExtensions)) {
                    System.out.println("The following are all files of type " + selectedExtension + ":");
                    System.out.println(fileService.listAllFileNamesByExtension(selectedExtension));
                }
                
                isValidSelection = true;
            } catch (InvalidUserSelectionException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
    }

    private void exploreTxtFiles(Scanner console) {
        String extension = ".txt";
        List<File> availableFiles = fileService.listAllFilesByExtension(extension);
        System.out.println("The following are all files of type " + extension + ":");
        System.out.println(fileService.listAllFileNamesByExtension(extension));

        System.out.println(
            "Select one of the above:"
        );

        String selectedFileName = null;
        boolean isValidSelection = false;
        while (!isValidSelection) {
            try {
                selectedFileName = console.nextLine();
                System.out.println();

                if (userService.validateUserFileNameSelection(selectedFileName, availableFiles)) {
                    File fileToExplore = retrieveFileToExplore(selectedFileName, availableFiles);
                    TxtFileController txtFileController = new TxtFileController(console, fileToExplore);
                }
                
                isValidSelection = true;
            } catch (InvalidUserSelectionException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
    }

    private File retrieveFileToExplore(String selectedFileName, List<File> availableFiles) {
        File fileToExplore = null;
        for (int i = 0; i < availableFiles.size(); i++) {
            File currFile = availableFiles.get(i);
            if (currFile.getName().equals(selectedFileName)) {
                fileToExplore = currFile;
            }
        }
        return fileToExplore;
    }

    public void displayMenuAndRepromt(Scanner console) {
        displayMainMenu();
        selectMenuItem(console);
    }
}
