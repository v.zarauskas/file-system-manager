package controller;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.Scanner;

import service.FileService;
import service.TxtFileService;
import service.UserService;
import exceptions.userExceptions.InvalidFormatException;
import exceptions.userExceptions.InvalidUserSelectionException;

public class TxtFileController {

    private TxtFileService txtFileService;

    WelcomeController welcomeController = new WelcomeController();
    UserService userService = new UserService();
    FileService fileService = new FileService();
    MenuController menuController = new MenuController();

    public TxtFileController(Scanner console, File file) {
        this.txtFileService = new TxtFileService(file);
        displayTxtFileMenu();
        selectMenuItem(console);
    }

    public void displayTxtFileMenu() {
        System.out.println(
            "You have the following options:" + "\n" +
            "   [q] or [Q] - exit the program" + "\n" +
            "   [r] - return to the main menu" + "\n" +
            "   [s] - switch user" + "\n" +
            "   [g] - get file size" + "\n" +
            "   [l] - calculate number of lines" + "\n" +
            "   [c] - determine if the file contains a given word" + "\n" +
            "   [w] - count occurence of a given word"
        );

    }

    public void selectMenuItem(Scanner console)  {
        System.out.println(
            "Select an option:"
        );

        String selectedMenuItem = null;
        boolean isValidMenuItemSelected = false;
        while (!isValidMenuItemSelected) {
            try {
                String menuOptions = "rsglcw";
                selectedMenuItem = console.nextLine();
                System.out.println();

                if (selectedMenuItem.equalsIgnoreCase("q")) {
                    System.exit(0);
                } else if (userService.validateUserMenuItemSelection(selectedMenuItem, menuOptions)) {
                    switch (selectedMenuItem) {
                        case "r":
                            menuController.displayMenuAndRepromt(console);
                            break;
                        case "s":
                            switchUser(console);
                            break;
                        case "g":
                            getFileSize(console);
                            break;
                        case "l":
                            calculateNumberOfLines(console);
                            break;
                        case "c":
                            containsWord(console);
                            break;
                        case "w":
                            countOccurenceOfAWord(console);
                            break;
                    }
                }
                
                isValidMenuItemSelected = true;
            } catch (InvalidUserSelectionException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
    }

    private void switchUser(Scanner console) {
        System.out.println("\nThe previous session is over.");
        welcomeController.getUserName(console);
        displayMenuAndRepromt(console);
    }

    private void getFileSize(Scanner console) {
        long fileSize = txtFileService.getFile().length() / 1024;
        System.out.println(
            "The file size is:" + "\n" +
            fileSize + "KB"
            );
        displayMenuAndRepromt(console);
    }

    private void calculateNumberOfLines(Scanner console) {
        int nrOfLines = txtFileService.calculateNrOfLines();
        System.out.println(
            "The number of lines in the file is:" + "\n" +
            nrOfLines
        );
        displayMenuAndRepromt(console);
    }

    private void containsWord(Scanner console) {
        String word = null;
        boolean isValidSelection = false;
        System.out.println("Type in a word to search for:");
        while (!isValidSelection) {
            try {
                word = console.nextLine();
                System.out.println();

                if (userService.validateUserWordInput(word)) {
                    if (txtFileService.containsWord(word)) {
                        System.out.println("The file DOES contain the word \"" + word + "\"\n" );
                    } else {
                        System.out.println("The file does NOT contain the word \"" + word + "\"n" );
                    }
                }
                
                isValidSelection = true;
            } catch (InvalidFormatException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
        displayMenuAndRepromt(console);
    }

    private void countOccurenceOfAWord(Scanner console) {
        String word = null;
        int nrOfOccurences = 0;
        boolean isValidSelection = false;
        System.out.println("Type in a word to search for:");
        while (!isValidSelection) {
            try {
                word = console.nextLine();
                System.out.println();

                if (userService.validateUserWordInput(word)) {
                    nrOfOccurences = txtFileService.countOccurenceOfAWord(word);
                    System.out.println("The file contains the word \"" + word + "\":" );
                    System.out.println(nrOfOccurences + " times\n");
                }
                
                isValidSelection = true;
            } catch (InvalidFormatException e) {
                System.err.println(e.getMessage());
            } catch (NoSuchElementException e) {
                //TODO: handle exception
            } catch (IllegalStateException e) {
                //TODO: handle exception
            }
        }
        displayMenuAndRepromt(console);
    }

    private void displayMenuAndRepromt(Scanner console) {
        displayTxtFileMenu();
        selectMenuItem(console);
    }
}
