## __Compilation Screenshot__
![Compilation Screenshot](/src/main/resources/screenshots/compilation.png?raw=true "Compilation Screenshot")


## __Packaging Screenshot__
![Packaging Screenshot](/src/main/resources/screenshots/packaging.png?raw=true "Compilation Screenshot")

## __Running .class Screenshot__
![Running .class Screenshot](/src/main/resources/screenshots/running class.png?raw=true "Running .class Screenshot")

NB: The screenshots for running the jar file are not provided because the access to resources has not been resolved yet. However, the program runs
